const express = require("express")
const router = express.Router()
const TaskController = require("../controllers/TaskController")

// Create new task
router.post("/", (request, response) => {
	TaskController.createTask(request.body).then(result => response.send(result))
})

// Get all  tasks
router.get("/", (request, response) => {
	TaskController.getAll().then(result => response.send(result))
})

// [NEW] Get a specific task
router.get("/:id", (request, response) => {
	TaskController.getAll().then(result => response.send(result))
})

// Update a specific task
router.put("/:id", (request, response) => {
	TaskController.updateTask(request.params.id, request.body).then(result => {
		response.send(result)
	})
})

// [NEW] Update a specific task's status
router.put("/:id/complete", (request, response) => {
	TaskController.updateTask(request.params.id, request.body).then(result => {
		response.send(result)
	})
})

// Delete a specific task
router.delete("/:id", (request, response) => {
	TaskController.deleteTask(request.params.id, request.body).then(result => response.send(result))
})

module.exports =  router