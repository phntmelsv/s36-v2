const Task =  require("../models/Task")

// Get all tasks in database
module.exports.getAll = () => {
	return Task.find({}).then(result => {
		return result
	})
}

// Get a specific task
module.exports.getTask = (task_id, content) => {
	return Task.findById(task_id).then((result, error) => {
		if(error) {
			return error
		}

		return result.save().then((specific_task,  error) =>  {
			if(error){
				return error
			}

			return specific_task
		})
	})

}

// Create a new task
module.exports.createTask = (request_body) => {
	let new_task = new Task({
		name: request_body.name
	})

	return new_task.save().then((created_task, error) => {
		if(error){
			return error
		}

		return created_task
	})
}

// Update a specific task's status
module.exports.updateTask = (task_id, new_content) => {
	return Task.findById(task_id).then((result, error) => {
		if(error) {
			return error
		}

		// Change a status of a task to complete
		result.status = new_content.status

		return result.save().then((updated_task,  error) =>  {
			if(error){
				return error
			}

			return updated_task
		})
	})

} 
